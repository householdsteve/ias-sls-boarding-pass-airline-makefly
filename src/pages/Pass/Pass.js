import React, { Component, useState, useEffect } from 'react';
import { Link, withRouter } from 'react-router-dom';

import Skeleton from 'react-loading-skeleton';
import { QRCode, useQRCode } from 'react-qrcode';

import axios from 'axios';

import dayjs from "dayjs";

import {
  searchPass,
} from '../../utils'

import {
  isBrowser,
	isIOS,
	isAndroid,
	isMobile,
	isFirefox,
	isEdge,
} from "react-device-detect";

import styles from './Pass.module.scss';

import { ReactComponent as APPLEWALLET } from '../../assets/images/Apple_Wallet.svg';

function successHandler(e) {
	console.log("success:", e);
}
function failureHandler(e) {
	console.log("failure:", e);
}

const serveDownload = (fileName,path,passType) => {
	if(passType === "application/pdf") {
		const link = document.createElement('a');
		link.href = path;
		link.target = "_blank";
		document.body.appendChild(link);
		link.click();
		document.body.removeChild(link);
		return true;
	}

	let rep;
	axios({
		url: path,
		method: 'GET',
		responseType: 'blob', // important
	}).then((response) => {
		
		//window.location.href = URL.createObjectURL(new Blob([response.data],{type: "application/vnd.apple.pkpass"})); 
		
		const reader = new FileReader();
		const out = new Blob([response.data],{type: passType});
		reader.onload = function(e){
				window.location.href = reader.result;
		}
		reader.addEventListener('error', () => {
            alert("error")
        });
		reader.readAsDataURL(out);
	});
}

const GooglePayTag = (props) => {
	useEffect(() => {
		const script = document.createElement("script");
		script.onload = function(e) {
			
			window.gapi.savetoandroidpay.render("googlePayPassHolder",{
				"jwt": props.jwt,
				"onsuccess": successHandler,
				"onfailure": failureHandler,
				"size":"matchparent",
				"textsize" : "large",
        "theme":"light"
			});
		};
    script.async = true;
		script.src = "https://apis.google.com/js/platform.js";
    document.body.appendChild(script);
	}, []);
	return (<div id="googlePayPassHolder"/>);
}

const BoardingPassView = (props) => {
	const [activeItemIndex, setActiveItemIndex] = useState(0);
	const boardingPassData = props.bpData;
	
		 return (<React.Fragment>
						 {boardingPassData.iOSPassTemplates.map((bp, index) =>
						 
						 <div key={index} id={`pass-${index}`} className={`${styles.child} col`}>
 
							 <div className={`${styles.card} w-100 py-3 shadow`} style={{backgroundColor: boardingPassData.passStyles.backgroundColor, color:boardingPassData.passStyles.labelColor}}>
								<div className="row px-3 mb-2">
									<div className="col-6">
										{/* <img className={`${styles.logo} img-fluid`} src={boardingPassData.logo} /> */}
										{<img className={`${styles.logo} img-fluid`} src={boardingPassData.logo} /> || <Skeleton count={2} width={150} />}
								 	</div>
									<div className="col-6 d-flex justify-content-between align-items-end">
									{bp.boardingPass.headerFields.map((f, index) => <div key={index} className="">
											<div>
												<small className="d-block text-uppercase">{f.label}</small>
												<span className="fs-12 d-inline-block text-nowrap overflow-hidden">{f.value}</span>
											</div>
										</div>)}
									</div>
								</div>
							 <div className="col-12 mt-2 d-flex justify-content-between align-content-center">
								 <div>
										<small className="d-block text-uppercase">{bp.boardingPass.primaryFields.find(p => p.key === "origin").label}</small>
										<h1 className="">{bp.boardingPass.primaryFields.find(p => p.key === "origin").value}</h1>
								 </div>
								 <div className={`${styles.plane} align-self-center iasi ic-hide ic-plane-af`}><i>✈️</i></div>
							 		<div>
									 	 <small className="d-block text-right text-uppercase">{bp.boardingPass.primaryFields.find(p => p.key === "destination").label}</small>
										 <h1 className="">{bp.boardingPass.primaryFields.find(p => p.key === "destination").value}</h1>
									 </div>
							 </div>
 
							 <div className="col-12 mt-3 d-flex justify-content-between">
									 {bp.boardingPass.auxiliaryFields.map((f, index) => <div key={index} className="">
										 <div>
											 <small className="d-block text-uppercase">{f.label}</small>
											 {f.value}
										 </div>
									 </div>)}
							 </div>
							 
							 <div className="col-12 mt-3 d-flex justify-content-between">
									 {bp.boardingPass.secondaryFields.map((f, index) => <div key={index} className="">
										 <div>
											 <small className="d-block text-uppercase">{f.label}</small>
											 {f.value}
										 </div>
									 </div>)}
							 </div>
							 <div className="mt-5 col-8 mx-auto d-flex justify-content-center">
							 {/* <img className={`rounded img-fluid`} src={dataUrl} /> */}
							 		
									 {<QRCode className={`rounded`} width="200" value={bp.barcode.message} />|| <Skeleton count={1} width={200} height={200} />}
							 </div>

							 
							 </div>
							 {(((isIOS && (!isFirefox && !isEdge)) || isBrowser) && (
							 		<div className="col-8 mt-3 mx-auto pointer" onClick={() => serveDownload(bp.serialNumber,boardingPassData.iOSPassCaches[index].Location,"application/vnd.apple.pkpass")}>
										{/* <a href={boardingPassData.iOSPassCaches[index].Location}> */}
											<APPLEWALLET className="pointer"/>
										{/* </a> */}
							 		</div>
							 ))}

							 {((isIOS && (isFirefox || isEdge)) && (
								 <div className="col-8 mt-3 mx-auto pointer">
										<h5 className="text-center w-100">Open this page on Safari or Chrome to add pass to your wallet.</h5>	
							 		</div>
							 ))}

									<div className="col-8 mt-3 mx-auto pointer" onClick={() => serveDownload(bp.serialNumber,boardingPassData.iOSPassCaches[index].Location.replace(".pkpass",".pdf"),"application/pdf")}>
										<h6 className="text-center mt-4 text-muted w-100">OR Download a PDF</h6>	
							 		</div>
								
 
						 	</div>
						 )}
					 </React.Fragment>
	)
}

const ViewOnMobileQRCode = (props) => {
	const mobileUrl = useQRCode({value: window.location.href, width:"500", margin: 0});
		 return (<img className={`rounded ${styles.barcode} img-fluid`} src={mobileUrl} />) 
}


class Pass extends Component {
	constructor(props) {
		super(props);
		this.state = {
			passData: null,
			isLoading: true
		};
	}

	async componentDidMount() {
		if(!this.props.match.params.id){
			this.setState({passData: { error:true }, isLoading: false});
			return
		}
		try {
			const data = await searchPass(this.props.match.params.id);
			this.setState({passData: data.pass, isLoading: false})
		} catch(e) {
			this.setState({passData: { error:true }, isLoading: false});
		}

	}
	
	render() {

		return (<React.Fragment>
			{(this.state.passData && !this.state.passData.error && (
				<div className={`animateFadeIn container-fluid pt-3 ${(isMobile && this.state.passData.iOSPassTemplates.length > 1) ? 'p-0' : ''}`}>
				
				<div className={`container ${isMobile ? 'p-0' : ''}`}>
			  <div className={`row`}>
				<div className={`col-12 mx-auto d-flex justify-content-start justify-content-sm-center`} >

					<div className={`${styles.passes} ${isMobile ? styles.mobile: ''} pb-3`} data-columns={this.state.passData.iOSPassTemplates.length}>
						<BoardingPassView bpData={this.state.passData} />
					</div>

					</div>
				</div>
				{((isAndroid || isBrowser) && (
					<div className={`row mt-2`}>
						<div className={`col-12 col-sm-4 col-md-4 mx-auto`} >
							{/* <h5 className="text-center w-100">Add on GooglePay wallet</h5> */}
							<GooglePayTag jwt={this.state.passData.googlePayPassJWT} />
						</div>
					</div>
				))}
				{(!isMobile && (
					<div className={`row mt-5 mb-5`}>
						<div className={`col-12 col-sm-4 col-md-4 mx-auto`} >
							<h6 className="text-center w-100 mb-3">Scan this QR code to open this page on your mobile device</h6>
							<ViewOnMobileQRCode passId={this.state.passData.passId} />
						</div>
					</div>
				))}
			</div>
				
			</div>
			
			)) || (this.state.isLoading && (
			
			<div className={`animateFadeIn container-fluid pt-3 p-0`}>
				
				<div className={`container ${isMobile ? 'p-0' : ''}`}>
			  <div className={`row`}>
				<div className={`col-10 mx-auto d-flex justify-content-start justify-content-sm-center`} >

					<div className={`${styles.passes} ${isMobile ? styles.mobile: ''}`}>
						<Skeleton count={1} height={450} className={`${styles.skeleton}`}/>
					</div>

					</div>
				</div>				
			</div>
				
			</div>  ))
			
			}

			{(this.state.passData && this.state.passData.error && (
				<div className="animateFadeIn container h-100">
					<div className="row h-100 justify-content-center align-items-center">
						<div>
							<h2 className="text-center w-100">Pass not found</h2>
							<p className="d-block p-3 text-center">Please contact us and we can assist with finding your digital boarding pass.</p>
						</div>
					</div>
				</div>
			))}

			</React.Fragment>
		);
	}
}

export default withRouter(Pass);
